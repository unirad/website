FROM ruby
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install && rm -rf Gemfile Gemfile.lock && jekyll new unirad

WORKDIR /unirad
