---
layout: page
title: Spenden
permalink: /donations
---
Unirad finanziert sich auschließlich durch Spenden.

Falls du uns unterstützen möchtest, kannst du uns also gerne etwas Geld zukommen lassen. Genauso freuen wir uns über Fahrräder oder Fahrradteile die du nicht mehr brauchst, an denen kann sich dann noch jemand anderes erfreuen.

Von dem Geld besorgen wir dann neue Werkzeuge und Verbrauchsmaterialien, wie Kettenöl, Zugendhülsen, Klebeband und vieles mehr.