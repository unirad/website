---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
#### Unirad ist eine **Selbsthilfewerkstatt** an der TU Berlin. Wir haben Mo, Di, Mi von 18 bis 21 geöffnet. Wir stellen das Werkzeug zur Verfügung und können euch hier und da Tipps zu euren Reperaturen geben. Die Reparaturen führt ihr aber in Eigenverantwortung durch. Wir haben auch viele gebrauchte und einige neue Teile.
