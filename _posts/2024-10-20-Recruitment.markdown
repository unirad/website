---
layout: post
title:  "Plenum + geschlossene Werkstatt am 29.Oktober 2024, Dienstag"
date:   2024-10-22 21:30:00 +0000
categories: announcments
---

![Plenumeinladung]({% link assets/plenumWISE2024.jpg %})

Hast du Lust mehr über Fahrräder und Reperaturen zu lernen und anderen zu Helfen: Dann mach' doch bei uns mit!
Wir haben am 29.10 um 20:00 Uhr unser Plenum, dort kannst du uns kennenlernen und alles erfahren, wie du bei uns mitmachen kannst. Du braucht gar keine Vorkenntnisse und Erfahrung und wirst alles im Laufe der Zeit erlernen. 

Die Werkstatt wird deswegen am 29.Oktober 2024 geschlossen bleiben.