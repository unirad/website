---
layout: page
title: Öffnungzeiten
permalink: /times
---
Unsere aktuelle Öffnungszeiten sind (Wintersemester 2024/25):
- Montag: 18:00 - 21:00
- Dienstag: 18:00 - 21:00
- Mittwoch: 18:00 - 21:00

Diese können sich aber kurztzeitig ändern. Wir haben in der Regel auch an Feiertagen geöffnet, für genauere Informationen schaut bitte auf der [Hauptseite]({% link index.markdown %}) nach.
