---
layout: page
title: FAQ
permalink: /faq
---

# Wer kann zu Unirad kommen?
Bei Unirad ist jeder willkommen.

# Kann man bei Unirad Fahrräder reparieren lassen?
Nein, Unirad ist eine Selbsthilfewerkstatt, d.h. der Besitzer repariert sein Rad selber, es werden keine Reparaturaufträge angenommen; dafür gibt es Fahrradgeschäfte und -werkstätten.

# Kann man bei Unirad einen platten Reifen beheben?
Natürlich, wir haben eine Auswahl neuer und gebrauchter Schläuche, gebrauchte Mäntel und natürlich auch Flickzeug und eine Luftpumpe.

# Kann man bei Unirad Fahrräder kaufen?
Nein, Unirad ist kein Fahrradhandel.

# Gibt es bei Unirad Ersatzteile?
Ja, wir haben einige oft verwendete Teile neu da und viele gebrauchte Teile. Eine Liste gibt es [hier]({% link parts.markdown %}).
Da wir nur von Spenden finanziert werden, wäre es nett, wenn bei Nutzung von Teilen eine Spende errichtet wird. Wie hoch diese Spende ausfallen soll, damit wir weiterhin unser Angebot anbieten können, ist je nach Teil unterschiedlich und kann vor Ort erfragt werden.

# Welche Werkzeuge habt ihr?
Wir haben Werkzeuge für so ziemlich jede erdenkliche Reperatur an einem Fahrrad. Von standart Werkzeugen wie Schraubendrehern, Gabelschlüsseln, Sägen und Hämmern, bis hin zu speziellerem Fahrradwerkzeugen, wie z.B. Kettennietern, Speichenschlüsseln, Kurbelabziehern, Tretlagerwerkzeugen, Kassettenabziehern. 
(Mehr dazu [hier]({% link misc/tools.markdown %}).)

# Bekommt ihr Geld?
Nein, wir arbeiten ehrenamtlich in unserer Freizeit, wir bekommen kein Geld, Aufwandsentschädigung oder Ähnliches.

# Wer sind die Leute von Unirad?
Wir sind Studierende aus verschiedenen Fachrichtungen und insbesondere sind wir keine ausgebildeten Mechaniker.
