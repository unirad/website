---
layout: page
title: Teile
permalink: /parts
---
Wir haben einige Teile auf Lager. Diese sind zum Teil gebraucht, zum Teil neu.

Da wir nur von Spenden finanziert werden, wäre es nett, wenn bei Nutzung von Teilen eine Spende errichtet wird. Wie hoch diese Spende ausfallen soll, damit wir weiterhin unser Angebot anbieten können, ist je nach Teil unterschiedlich und kann vor Ort erfragt werden.

## Gebrauchte Teile
- Schläuche
- Mäntel
- Felgen
- verschiedene Narben
- Kassetten
- Kurbeln
- Pedale
- Schaltwerke
- Kassetten
- Umwerfer
- Bremsen
- Bremshebel
- Schalthebel
- Sättel
- Tretlager
- verschiedene Kleinteile, wie z.B. Schrauben, Muttern, Ventilkörper, Ventilkappen, Kugelkäfige
- Lampen
- Reflektoren
- Gepäckträger
- Kettenschutzbleche
- Schutzbleche
- Speichen
- Speichennippel
## Neue Teile
- Schläuche
- Tretlager
- Ketten
- Kettenschlösser
- Bremsbeläge
- Bremszüge
- Schaltzüge
- Kugeln
- Kabel
- Glühbirnen
