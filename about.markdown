---
layout: page
title: Über Uns
permalink: /about
---

Unirad ist eine **Selbsthilfewerkstatt**, dass heißt, Ihr müsst euer Fahrrad **selbst reparieren**. Wir stellen das Werkzeug zur Verfügung und können euch hier und da Tipps zu euren Reperaturen geben. Die Reparaturen führt ihr aber in Eigenverantwortung durch.


Nun sind wir aber keine ausgemachten Schweine, die hämisch grinsend zusehen, wie ihr euch vergeblich mit irgendwelchen kniffligen Sachen rumärgert; vielmehr sind wir auch gern bereit, mit Hand anzulegen.
**Verantwortlich seid jedoch ihr selbst**.


Und wir übernehmen auch keine Reparaturen. Falls ihr euer Fahrrad nicht selbst reparieren wollt, lohnt sich für euch auch nicht der Weg zu Unirad.
Dann geht lieber zum Fahrradhändler eures Vertrauens.

Falls euch Unirad gefällt würden wir uns um eine Spende freuen, mit dieser besorgen wir neue Werkzeuge und Verbrauchsmaterialien, näheres [hier]({% link donations.markdown %}).
