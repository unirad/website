---
layout: page
permalink: /misc/tools
---
# Werkzeug
Wir haben Werkzeuge für so ziemlich jede erdenkliche Reperatur an einem Fahrrad. Von standart Werkzeugen wie Schraubendrehern, Gabelschlüsseln, Sägen und Hämmern, bis hin zu speziellerem Fahrradwerkzeugen, wie z.B. Kettennietern, Speichenschlüsseln, Kurbelabziehern, Tretlagerwerkzeugen, Kassettenabziehern.
Es ist vermutlich einfacher alle Werkzeuge aufzulisten, nach denen wir schon gefragt wurden, oder die wir brauchten, aber nicht haben.

## Werkzeuge, die wir *nicht* haben
- Framing Tisch für Fahrradrahmen
- TIG/WIG Schweißgerät für Aluminium, Titan
- Außen- und Innengewindeschneider für Vollachsen in FG7.9,FG9.5 und Shimano Narben in 3/8'', 3/16'', und 7/32'' 
- Drehbank
- Fräse
