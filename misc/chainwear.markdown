---
layout: page
permalink: /misc/chainwear
---
# Kettenverschleiß
## Warum ölen?
Wenn Deine Kette nicht gut geölt ist, quietscht sie und rostet sie, außerdem geht zunehmend Kraft beim radeln verloren, wenn sich die Kettenglieder nicht mehr gut bewegen lassen. Wenn die Kette rostet, heißt das, das zu viel Wasser und zu wenig Öl dran war. Beim Ölen transportierst Du jedoch auch Dreck tief in die Kette hinein, wo er eigentlich nicht hingehört.

Der Sand und Dreck, der auf der Kette klebt, wandert bei der ständgen Kettenbewegung langsam nach innen in die Gelenke hinein und trägt dort zum Kettenverschleiß bei. Der Kettenverschleiß einer rostigen, quietschenden Kette ist natürlich auch um ein Vielfaches höher als der einer gut gepflegten Kette.

Außen braucht die Kette nicht unbedingt ölig zu sein, zuviel Öl an der Außenseite wird nur Staub und Sand von der Straße oder dem Waldweg anziehen. Außerdem dankt es Deine Hose, die dann nicht so schnell schmierig wird, wenn sie an die Kette kommt.

Wenn Du Deine Kette häufiger ölst und reinigst, hält sie auch länger.

Denn die Kette des Fahrrades ist ein Verschleißteil. Durch die starke Belastung beim Vorwärtstreten wird die Kette allmählich immer länger und länger
Von Zeit zu Zeit (alle 1000km) sollte daher die Kette kontrolliert und gegebenenfalls gewechselt werden, um den Verschleiß der anderen Antriebsteile: Ritzel (das sind die kleinen Zahnräder am Hinterrad) und Kettenblätter (die großen Zahnräder vorn am Tretlager, wo die Pedale angebracht sind) zu reduzieren.
Nur wenn die Kette rechtzeitig gewechselt wird, kann ein Wechsel der Ritzel und/oder Kettenblätter vermieden werden und nur dann. Wenn es schon kracht beim Anfahren an der Kreuzung dann wird ein Austausch aller drei Komponenten (Kette, Ritzel und Kettenblätter) sehr viel teurer.

## Wie ölen und reinigen?
Zum ölen und reinigen brauchst du nicht viel; ein alter Lappen und Kettenöl reichen. Besser geht es aber wenn du noch eine Drahtbürste, eine Zahnbürste und eine alte Spülbürste hast.

Mit den Bürsten oder dem Lappen fängst du damit an den groben Dreck von der Kette und allem was dise berührt zu enfernen. Wenn der dieser Dreck sehr hartneckig ist kann auch ein Schraubenzieher helfen.

Wenn der grobe Dreck weg ist, kannst du die Kette mit dem Lappen und dem Kettenöl reinigen. Dazu tust du zuerst etwas Kettenöl auf die Kette. Dann nimmst du den Lappen und wickelst ihn um die Kette. Und Tritts die Kette mit der Hand durch den Lappen. Wenn du das ein paar mal gemacht hast, ist die Kette sauber. Hier können auch wieder die Bürsten helfen.

Nun musst du nur noch die Kette ölen. Dazu tust du wieder etwas Kettenöl auf die Kette und trittst sie durch.

Zuletzt solltest du noch das überschüssige Öl von der Kette entfernen. Dazu kannst du den Lappen nehmen und die Kette damit abwischen.
Du solltest am ende kein Öl mehr auf der Kette sehen.

## Wie kann man Verschleiß messen?
Um den Verschleiß einer Kette zu messen brauchst du ein Kettenmessgerät. Das ist ein kleines Werkzeug, das du in die Kette einhängst. Wenn die Kette verschlissen ist, fällt das Werkzeug in die Kette. Dann *solltest* du die Kette austauschen. Natürlich haben wir so ein Werkzeug in der Werkstatt.

## Wie sieht Verschleiß aus?
![Kette_gelaengt-klein.jpg]({% link /assets/Kette_gelaengt-klein.jpg%})

Die linke der beiden Fahrradketten auf dem Bild ist eine neue Kette. Nach einigen Kettengliedern kann man sehen, dass die Alte kette einen größeneren abstand zischen den einzelen Kettengliedern hat . Dass eine solche Kette nicht mehr in die für eine neue Fahrradkette ausgelegten Zwischenräume der Zähne an Ritzel (so werden die am Hinterrad befestigten meist kleineren Zahnkränze genannt) oder  am Kettenblatt (die vorne mit den Pedalen verbundenen großen Zahnkränze) passt, dürfte klar sein.
Was macht also die Kette, wenn sie da, wo sie hinein gehen sollte, nicht mehr hineinpasst? Sie hatte, bevor sie so lang geworden ist wie die hier oben gezeigte einige Zeit die Zahnzwischenräume an  Ritzeln und Kettenblättern ihrer neuen Länge anzupassen.
Die Kette wird ja nicht schlagartig länger, sondern der Verschleiß der Kette und Längung der Kette setzt langsam, ganz allmählich ein und ist in seinem Verlauf auch sehr von Fahrstil (Stichwort: Kettenlinie), Umwelteinflüssen und Kettenpflege abhängig.
Aber in jedem Fall passt die Kette - während sie sich längt - zunächst die Zwischenräume der Zähne an den Ritzeln ihrer neuen Länge an, damit sie wieder richtig dort hineinpasst. Das Ende dieses Prozess sieht in etwa so aus:

![Ritzel_verschlissen-klein.jpg]({% link /assets/Ritzel_verschlissen-klein.jpg%})

## Mehr Informationen zu Kettenverschleiß (Englisch)
- [Zero Friction Cycling](https://zerofrictioncycling.com.au)
- [Sheldon Brown](https://www.sheldonbrown.com/chains.html)
