---
layout: page
permalink: /misc/rentals
---
# Leihräder
Bei uns kannst du dir auch ein Rad ausleihen. Dafür musst du nur eine Kaution von 50€ hinterlegen und zahlst dann eine Miete von 10€ pro Woche. Zum Ausleihen musst du einen Ausweis mitbringen, sowie die Kaution und Miete im Vorraus.
