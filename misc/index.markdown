---
layout: page
title: Sonstiges
permalink: /misc/
---

- [Best of WTF]({% link misc/wtf.markdown %})

- [Werkzeug]({% link misc/tools.markdown %})
- [Fahrradinspektion]({% link misc/inspection_checklist.markdown %})
- [Andere Selbsthilfewerkstätten (extern)](https://fahrrad.fandom.com/de/wiki/Fahrradselbsthilfewerkst%C3%A4tten_in_Berlin)
    - [hubSchrauber (extern)](http://www.refrat.de/hubschrauber/)
    - [FUrad (extern)](https://userblogs.fu-berlin.de/furad/)
- [Leihräder]({% link misc/rentals.markdown %})
- Anleitungen
    - [Wie heißt das Teil?]({% link misc/names.markdown %})
    - [Kettenverschleiß]({% link misc/chainwear.markdown %})
    - [Reperaturanleigunsvideos zu Tretlagern und Narbenschaltungen (extern)](https://www.youtube.com/@nlz.fahrrad/videos)
    - [Reperaturanleitungen (extern, englisch)](https://www.sheldonbrown.com/repairs.html)
    - [Infos zu Fahrrädern für Anfanger (extern, englisch)](https://www.sheldonbrown.com/beginners.html)
    - [Verschidene Anleitungen (extern, deutsch)](https://radtechnik.2ix.de/)
    - [Teile Kaufen]({% link misc/parts_shops.markdown %})
    - [Smolik Christian (extern)](http://www.smolik-velotech.de/)
        - [Grundlagen Fahrradtechnik (extern)](http://www.smolik-velotech.de/technik/index.htm)
        - [Glossar Fahrradtechnik/Stichwortsammlung (extern)](http://www.smolik-velotech.de/glossarfr.htm)
        - [Fahrradtuning 1989 (extern)](https://archive.org/download/smolik-christian-fahrradtuning-auflage-1989/Smolik_Christian_Fahrradtuning_Auflage_1989.pdf)
- [Warum lösen sich Schrauben (extern)](https://www.youtube.com/watch?v=7FdUCTvOjA0)
- Wie Baut Man Ein Fahrrad (Sendung Mit Der Maus, extern)?
    - [Teil 1, Fahrradrahmen](https://www.youtube.com/watch?v=FnbuGblnMAk)
    - [Teil 2, Laufräder](https://www.youtube.com/watch?v=GZCD-e3rXS4&t=314s)
    - [Teil 3, Bremsen und Freilaufritzel](https://www.youtube.com/watch?v=97SFhrpCGRY)
    - [Teil 4, Kasette, Schltwerk und Nabenschlatung](https://www.youtube.com/watch?v=FSfInF-2-Nc)
- [Unirad im TOR (extern)](http://uniradxlbqrnnwsifs3k5uw3emlb37qeulvafy6ggxbtk2ambixrzgad.onion/)
