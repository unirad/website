---
layout: page
permalink: /misc/parts_shops
---
# Teile Kaufen
Wenn du Teile für dein Fahrrad brauchst kann es sich lohnen zuerst bei [hubSchrauber](http://www.refrat.de/hubschrauber/) oder [FUrad](https://userblogs.fu-berlin.de/furad/) zu gucken, ob die dir nicht weiterhelfen können, falls du bei uns nicht fündig wurdest.

Natürlich kannst du auch zu deinem Fahrradladen um die Ecke gehen. Machne Teile sind da aber teuer oder schlichtweg nicht zu finden, darum sind hier ein paar online Shops, die gut sotiert sind:
- [decathlon.de](https://www.decathlon.de/alle-sportarten-a-z/fahrrad-welt)
- [bike24.de](https://www.bike24.de/)
- [bike-components.de](https://www.bike-components.de/)
- [bike-discount.de](https://www.bike-discount.de/)
- [velo-classic.de](https://velo-classic.de/oxid2)
