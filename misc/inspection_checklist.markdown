---
layout: page
permalink: /misc/inspection_checklist
---
# Fahrradinspektion

<ul id="toc">

</ul>

# Bremsen
Die Bremsen an deinem Fahrrad müssen immer einwandfrei funktionieren, falls du dir also die Kontrolle dieser nicht zutraust solltest du sie von jemand anderem Prüfen lassen. Die StVZO schreibt vor, dass dein Fahrrad [zwei unabhängige Bremsen hat](https://www.gesetze-im-internet.de/stvzo_2012/__65.html).

## Funktion der Bremsen
Um die Funktion der Bremsen zu prüfen musst du mit deinem Fahrrad fahren. Und dann jeweils mit einer der Bremsen bremsen. Dabei solltest du darauf achten, dass...
- der Bremshebel von deinen Fingern gut erreicht werden kann
- der Bremshebel nicht bis zum Lenker durchgezogen werden kann
- die Bremse dich spürbar und kraftvoll abbremst
- die Bremse dosiert werden kann

## Bremsbeläge
Bei deinen Bremsbelägen ist zu Prüfen, dass...
- sie korrekt positioniert sind, d.h. dass sie die Felge vollständig abdecken aber nicht an deinem Mantel schleifen einen möglichst kleinen Abstand zur Felge haben
- sie nicht abgenutzt sind, dass kannst du daran erkennen dass sie Rillen haben die Senkrecht zur Laufrichtung verlaufen
- sie gleichmäßig abgenutzt sind

## Felgen
Auch wichtig für die Funktion deiner Bremse sind die Felgen. Wenn deine Felge verschmutzt oder ölig ist kann deine Bremse nicht optimal funktionieren.
Auch hier solltest du prüfen, dass die Felge nicht abgenutzt ist. Einige Felgen haben dazu eine Rille, wenn diese nicht mehr zu sehen ist, ist die Felge abgenutzt. Anderenfalls kann man Abnutzung daran erkenne, dass die Felge eine Vertiefung hat. 
# Laufräder
## Luftdruck
Dein Rad braucht natürlich den korrekten Luftduck. Als grober Test ob dieser stimmt kannst du probieren seitlich deinen Mantel mit deiner Hand einzudrücken. Falls dies nur minimal möglich ist, ist der Luftdruck in Ordnung.
Bei einigen Mänteln und den meisten Schleuchen ist der korrekte Luftdruck aufgedruckt.
Falls dies nicht der Fall ist kannst du dich daran orientieren, dass der Luftdruck bei einem normalem Stadtrad zwischen 3 und 4 bar liegt.

## Mäntel
### Profil
Das Profil deines Mantels verschwindet mit der Zeit. Oftmals kann man mit einem Vergleich von dem vorderem und hinterem Mantel festellen, ob einer der Mäntel abgenutzt ist. Meist ist der hintere Mantel stärker abgenutzt als der vordere. Ein Tauschen der beiden kann also sinnvoll sein.

### Beschädigungen
Alte Mäntel neigen dazu porös zu werden. Das kann dazu führen, dass sie Risse bekommen. Diese Risse können dazu führen, dass der Schlauch platzt. Oder du schlichtweg schneller einen Platten bekommst.

### Laufrichtung
Gerne wird die Laufrichtung von Mänteln vertauscht. Das kann dazu führen, dass der Mantel schneller abgenutzt wird. Außerdem kann es dazu führen, dass der Mantel nicht mehr optimal bremst. Viele Mäntel habe die Laufrichtung aufgedruckt. Falls dies nicht der Fall ist, kannst du dich an dem Profil selber orientieren. Oft ist dies V-Förmig und zeigen in die Laufrichtung.

## Speichen
### Alle vorhanden?
Einfach zu kontrolieren ist, ob alle Speichen vorhanden sind. Dazu kannst du entlang der Felge prüfen, ob alle Speichen vorhanden sind. Falls nicht, solltest du diese ersetzen.

### Spannung
Genauso wie fehlende Speichen ein Problem sind, so sind auch Speichen mit falscher Spannung ein Problem. Tendenziell sollten alle Speichen gleichmäßig gespannt sein. Dies kannst du Prüfen in dem du jeweils zwei Speichen aneinander drückst.

## Zentrierung
Die Zentrierung deiner Felge kannst du Prüfen, indem du dein Laufrad in deinem Rahmen drehst. Mit einem Stift oder ähnlichem kann man dann vestellen, ob die Felge von rechts nach links oder von oben nach unten "eiert".

## Schleifen?
Einfach zu prüfen ist ob dein Laufrad schleift. Dazu musst du es einfach drehen. Falls etwas schleift kann man das meist hören oder fühlen. Ein scharfer Blick kann auch helfen.

# Lager
Ein Fahrrad hat einige Lager. Diese sorgen dafür, dass sich ein Teil, wie z.B. deine Ränder, mit möglichst wenig Reibung drehen kann.
Die Lager die du an deinem Fahrrad prüfen solltest sind:
- Tretlager
- Nabenlager
- Pedallager
- Steuersatzlager

## Lagerspiel
Damit das Lager frei laufen kann braucht es einen gewissen Grad an Luft zwischen seinen Bauteilen. Dies Luft nennt man das "Spiel". Ein Lager das korrekt eingestellt ist soltle kein oder nur sehr minimal spürbares Spiel haben. Das Spiel prüfst du, indem du probierst das Lager senkrecht zur Drehrichtung zu bewegen.
Um das Spiel am Steuersatz zu prüfen empfiehlt es sich die Vorderradbremse zu betätigen und dann das Rad am Lenker vor und zurück zu bewegen.

## Lagergeräusche und Vibrationen
Der Verschleiß von Lagern kündigt sich meist durch Vibrationen und Geräusche an. Um geräusche besser hören zu können kann man einen Schraubendreher oder eine Stift nehmen. Dazu drückt man ein Ende an sein Ohr (nicht in dein Ohr! Am besten z.B. an deinen Tragus) und das andere Ende hält man an verschiedene Stellen in Nähe zum Lager. 

## Rastpunkte
Rastpunkte treten eigentlich nur im Steuersatz auf. Hierbei bewegt sich das Lager nicht mehr gleichmäßig sondern rastet an bestimmten Punkten ein.

# Antrieb
## Zahnräder
Bei Zahnrädern ist zu prüfen, dass...
- sie die korrekte Zahnform haben, d.h. i.d.R., dass die Zähne nicht Spitz sind und nicht eine Sägezahnform haben
- die nicht verbogen sind
- keine Zähne fehlen

## Schaltung
- Können alle Gänge geschaltet werden?
- Springt die Schaltung in keinem Gang, auch unter Last?
- Sind die Begrenzungsschrauben korrekt eingestellt?

## Freilauf
- Ist der Freilauf leichtgängig?

## Kette
- Ist die Kette sauber?
- Ist die Kette geölt?
- Ist die Kette, gelängt, d.h. ist der Abstand zwischen den Kettengliedern (entlang der "Richtung" der Kette) nicht zu groß?
- Ob die Kette rostig ist, ist idr. egal.
- Stimmt die Kettenspannung?

# Züge
Bei den Zügen ist zu prüfen, ob
- die Enden korrekt abgeschlossen sind
- die Klemmstellen nicht ausgefranst sind
- die Züge nicht gerissen sind
- die Züge nicht korrodiert sind.
Das solltest du bei denen Brems- und Schaltzügen prüfen.

# Rahmen
Der Rahmen sollte keine Dellen oder Risse aufweißen. Das gilt besonders für Karbon-, Alu- und Titanrahmen.
Dafür können Stahlrahmen rosten.

# Lichtechnische Einrichtungen
Dein Fahrrad muss lichttechnische Einrichtungen haben. Diese sind:
- Lampen vorne und hinten
- Reflektoren vorne, hinten und an den Pedalen
- Mindestens zwei Katzenaugen pro Laufrad, je ein Reflektor an jeder des Laufrades oder kreisförmige Reflektorstreifen am Mantel

# Sonstiges
Außerdem solltest du noch prüfen, dass...
- Dein Fahrrad eine Klingel hat.
- Alle Schrauben, die fest sein sollten, auch fest sind. Manche Schrauben sollen nämlich nicht fest sein, da sie etwas einstellen. Diese erkennst du meist daran, dass sie relativ klein.
- Sattelstangen und Sattelstangen korrodieren gerne zusammen. Darum ist es empfehlenswert diese mit etwas Fett zu schmieren. Oder diese regelmäßig zu bewegen.
- Falls du dein Fahrrad nicht nur als Deko deine Wohnung smückt, solltest du dir deine Rahmennummer aufschreiben. Diese findest du meistens auf der Unterseite deines Tretlagers. Falls dein Fahrrad gestohlen wird, kannst du damit bei der Polizei Anzeige erstatten und hast dann wenigstens ein kleine Chance, dass dein Fahrrad wieder zu dier findet. 

<script async>
    const postContent = document.getElementsByClassName('post-content')[0];
    let children = [...postContent.children];
    children.shift();

    const tocObject = {};
    const current = {};
    children.forEach((elm) => {
        if (elm.tagName === 'H1') {
            tocObject[elm.innerText] = {};
            current.h1 = elm.innerText;
        }
        if (elm.tagName === 'H2') {
            tocObject[current.h1][elm.innerText] = {};
            current.h2 = elm.innerText;
        }
        if (elm.tagName === 'H3') {
            tocObject[current.h1][current.h2][elm.innerText] = {};
            current.h3 = elm.innerText;
        }
    });
    
    function onChange(event){
        const target = event.target;
        window.localStorage.setItem(target.name, target.checked);
    }

    function iterateTocObj(obj, ul){
        for(const key in obj)
        {
            const canonName = key.toLowerCase().replace(/ /g, '-');

            const li = document.createElement('li');
            
            const checkbox = document.createElement('input');
            checkbox.type = 'checkbox';
            checkbox.name = canonName;
            checkbox.className = 'task-list-item-checkbox';
            checkbox.checked = window.localStorage.getItem(canonName) === "true";
            checkbox.addEventListener("change", onChange);

            li.appendChild(checkbox);
            const a = document.createElement('a');
            a.innerText = key;
            a.href = '#' + canonName;
            li.appendChild(a);
            
            if(Object.keys(obj[key]).length !== 0)
            {
                const newUl = document.createElement('ul');
                li.appendChild(newUl);
                iterateTocObj(obj[key], newUl);
            }
            
            ul.appendChild(li);
        }
    }
    iterateTocObj(tocObject, document.getElementById('toc'));
</script>
