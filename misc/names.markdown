---
layout: page
permalink: /misc/names
---
![Bicycle diagram]({% link assets/Bicycle_diagram-dn.svg %})
(Aus der Wikimedia Commons)
- [Englische Version (extern)](https://commons.wikimedia.org/w/index.php?title=File:Bicycle_diagram-en.svg&lang=en)
- [Andere Sprachen (extern)](https://en.wikipedia.org/wiki/File:Bicycle_diagram-en.svg)
